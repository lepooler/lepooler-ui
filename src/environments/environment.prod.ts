export const environment = {
    production: true,
    jwtWhitelistedDomain: /.*REPLACED_BY_ENV_JWT_WHITELISTED_DOMAIN/,
    websocketsBaseUrl: 'REPLACED_BY_ENV_API_BASE_URL/',
    apiBaseUrl: 'REPLACED_BY_ENV_API_BASE_URL/api/v1/',
    tokenStorageName: 'access_token',
    spectatingTokenStorageName: 'spectating_token'
};
