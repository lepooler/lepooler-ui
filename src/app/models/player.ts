export class Player {
  constructor(value: Partial<Player>) {
    Object.assign(this, value);
  }

  public playerNhlId: string;
  public position: string;
  public name: string;
  public team: string;
  public teamAbbreviation: string;
  public age: number;
  public height: string;
  public weight: number;
  public salary: number;
  public predictions: PlayerPredictions[];
  public injuries: Injury[];

  public get activeInjuries() : Injury[] {
    return this.injuries.filter(i => !i.endDate || (i.startDate <= new Date() && i.endDate > new Date()));
  }

  // Properties not received from backend; filled by frontend in player selection
  public picked: boolean;
  public pickedBy: string;
}

export class PlayerPredictions {
  public rank: number;
  public gamesPlayed: number;
  public goals: number;
  public assists: number;
  public victories: number;
  public defeats: number;
  public shutouts: number;
  public overtimeDefeats: number;
  public victoriesLastYear: number;
  public defeatsLastYear: number;
  public shutoutsLastYear: number;
  public overtimeDefeatsLastYear: number;
  public points: number;
  public pointsPerMatch: number;
  public gamesPlayedLastYear: number;
  public goalsLastYear: number;
  public assistsLastYear: number;
  public pointsLastYear: number;
}

export class Injury {
  public playerNhlId: string;
  public startDate: Date;
  public endDate: Date;
  public description: string;
}
