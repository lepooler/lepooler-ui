export class Pool {
  constructor(value: Partial<Pool>) {
    Object.assign(this, value);
  }

  public poolGuid: string;
  public name: string;
  public season: string;
  public nbGoalerPicks: number;
  public nbOffenseDefensePicks: number;
  public pickTimeLimitInSeconds: number;

  // Properties not received from backend; filled by frontend in player selection
  public totalRounds: number;

  private _currentRound: number;
  public get currentRound() {
    return Math.min(this._currentRound, this.totalRounds);
  }
  public set currentRound(value: number) {
    this._currentRound = value === Infinity ? 0 : value;
  }
}
