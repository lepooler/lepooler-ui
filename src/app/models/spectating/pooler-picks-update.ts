import { Player } from '../player';

export class PoolerPicksUpdate {
    public poolerGuid: string;
    public poolerPicks: Player[];
    public pickedPlayerId: string;
}
