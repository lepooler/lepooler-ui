import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { CurrentPoolDataService } from '../current-pool-data-service/current-pool-data.service';
import { Player } from '../models/player';
import { Pool } from '../models/pool';
import { PoolParticipant } from '../models/pool-participant';
import { PoolerPicksUpdate } from '../models/spectating/pooler-picks-update';
import { PoolService } from '../pool-service/pool.service';

@Component({
    selector: 'app-player-selection-report',
    templateUrl: './player-selection-report.component.html'
})
export class PlayerSelectionReportComponent implements OnInit, OnDestroy {
  public poolParticipants: PoolParticipant[];
  public loadingReport = true;
  public isSpectating = false;
  public pickingPoolerGuid: string;
  public nextPickingPoolerGuid: string;

  private routeSnapshot: ActivatedRouteSnapshot;

  private subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private router: Router,
    private poolService: PoolService, private currentPoolDataService: CurrentPoolDataService) {
      this.routeSnapshot = this.route.snapshot;
  }

  ngOnInit() {
      this.isSpectating = this.routeSnapshot.data.isSpectating || this.currentPoolDataService.isSpectating;

      this.loadingReport = true;
      this.subscriptions.push(this.poolService.getPoolInfo().subscribe((currentPool: Pool) => {
          this.currentPoolDataService.setCurrentPool(currentPool, this.isSpectating || this.currentPoolDataService.isSpectating);
      }));
      this.subscriptions.push(this.poolService.getPoolParticipantsWithPickOrder().subscribe((poolParticipants: PoolParticipant[]) => {
          this.poolParticipants = poolParticipants;
          this.subscriptions.push(this.poolService.getAllPicksInPool().subscribe((picks: { [poolerGuid: string]: Player[] }) => {
              this.poolParticipants.forEach((poolParticipant: PoolParticipant) => {
                  if (picks[poolParticipant.pooler.poolerGuid]) {
                    poolParticipant.picks = picks[poolParticipant.pooler.poolerGuid];
                  }
              });
          }));
          this.loadingReport = false;

          if (this.isSpectating) {
              this.subscriptions.push(this.poolService.spectatePicks$.subscribe((poolerPicksUpdate: PoolerPicksUpdate) => {
                  const pooler = this.poolParticipants.filter((p: PoolParticipant) => p.pooler.poolerGuid === poolerPicksUpdate.poolerGuid);
                  if (pooler.length > 0) {
                      pooler[0].picks = poolerPicksUpdate.poolerPicks;
                  }
              }));
              this.subscriptions.push(this.poolService.spectatePickingPoolParticipant$.subscribe((poolerGuid: string) => {
                  this.pickingPoolerGuid = poolerGuid;
              }));
              this.subscriptions.push(this.poolService.spectateNextPickingPoolParticipant$.subscribe((poolerGuid: string) => {
                  this.nextPickingPoolerGuid = poolerGuid;
              }));
              this.poolService.connectToSpectateHub();
          }
      }));
  }

  ngOnDestroy() {
      this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  public print(): void {
      window.print();
  }

  public goToPlayerSelection(): void {
      if (this.isSpectating) {
          this.router.navigate(['spectate']);
      } else {
          this.router.navigate(['playerSelection']);
      }
  }
}
