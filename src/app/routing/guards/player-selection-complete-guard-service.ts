import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { PoolParticipant } from '../../models/pool-participant';
import { PoolService } from '../../pool-service/pool.service';

@Injectable()
export class PlayerSelectionCompleteGuard implements CanActivate {

    constructor(private router: Router,
        private poolService: PoolService,
        private toastr: ToastrService) { }

    async canActivate(): Promise<boolean> {
        const poolersInPool = await this.poolService.getPoolParticipants().toPromise().then((poolParticipants: PoolParticipant[]) => poolParticipants);
        if (poolersInPool.length !== 0) {
            const nextPoolersToPick = await this.poolService.getNextPoolParticipantsToPick('1').toPromise().then((nextPoolersToPickTemp: Map<number, string>) => nextPoolersToPickTemp);
            if (nextPoolersToPick === undefined || nextPoolersToPick[1] === undefined) {
                return true;
            }
        }
        this.router.navigate(['/playerSelection']);
        this.toastr.error('Le rapport ne peut pas être affiché si la sélection des joueurs n\'est pas complétée.');
        return false;
    }
}
