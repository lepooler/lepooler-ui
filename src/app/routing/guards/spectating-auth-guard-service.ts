import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { environment } from '../../../environments/environment';
import { AuthGuard } from './auth-guard-service';

@Injectable()
export class SpectatingAuthGuard extends AuthGuard implements CanActivate {
    protected cannotActivateMessage = 'Lien invalide';

    public canActivate(): boolean {
        if (localStorage.getItem(environment.spectatingTokenStorageName)) {
            return true;
        }
        return this.authenticationRequired();
    }
}
