import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { KeyValuePair } from '../common/key-value-pair';
import { Utilities } from '../common/utilities';
import { Player } from '../models/player';
import { Pool } from '../models/pool';
import { PoolParticipant } from '../models/pool-participant';
import { Pooler } from '../models/pooler';
import { PoolerPicksUpdate } from '../models/spectating/pooler-picks-update';

@Injectable({
  providedIn: 'root',
})
export class PoolService {
  private spectatePoolParticipants = new Subject<PoolParticipant[]>();
  public spectatePoolParticipants$ = this.spectatePoolParticipants.asObservable();

  private spectatePicks = new Subject<PoolerPicksUpdate>();
  public spectatePicks$ = this.spectatePicks.asObservable();

  private spectatePickingPoolParticipant = new BehaviorSubject<string>(undefined);
  public spectatePickingPoolParticipant$ = this.spectatePickingPoolParticipant.asObservable();

  private spectateNextPickingPoolParticipant = new BehaviorSubject<string>(undefined);
  public spectateNextPickingPoolParticipant$ = this.spectateNextPickingPoolParticipant.asObservable();

  private spectatePicksCompleted = new BehaviorSubject<boolean>(false);
  public spectatePicksCompleted$ = this.spectatePicksCompleted.asObservable();

  private spectateHubConnection: signalR.HubConnection;
  private spectateHubConnectionOptions: signalR.IHttpConnectionOptions = {
    accessTokenFactory: () => {
      return Utilities.getJwtToken();
    },
  };

  constructor(private http: HttpClient, private toastr: ToastrService) {}

  public getSeasons(): Observable<string[]> {
    return this.http.get<string[]>(this.getFullApiUrl('seasons'));
  }

  public authenticate(authenticationData: any): Observable<string> {
    return this.http.post(this.getFullApiUrl('pools/authenticate'), JSON.stringify(authenticationData), { responseType: 'text' });
  }

  public authenticateSpectating(poolGuid: string): Observable<string> {
    return this.http.get(this.getFullApiUrl(`pools/authenticateSpectating/${poolGuid}`), { responseType: 'text' });
  }

  public createPool(poolData: any): Observable<string> {
    return this.http.post(this.getFullApiUrl('pools'), JSON.stringify(poolData), { responseType: 'text' });
  }

  public getPoolInfo(): Observable<Pool> {
    return this.http.get<Pool>(this.getFullApiUrl('pools/info')).pipe(map((pool: Pool) => new Pool(pool)));
  }

  public getPoolParticipants(): Observable<PoolParticipant[]> {
    return this.http
      .get<PoolParticipant[]>(this.getFullApiUrl('pools/poolParticipants'))
      .pipe(map((poolParticipants: PoolParticipant[]) => poolParticipants.map((poolParticipant: PoolParticipant) => new PoolParticipant(poolParticipant))));
  }

  public getPoolParticipantsWithPickOrder(): Observable<PoolParticipant[]> {
    return this.http
      .get<PoolParticipant[]>(this.getFullApiUrl('pools/pickOrder'))
      .pipe(map((poolParticipants: PoolParticipant[]) => poolParticipants.map((poolParticipant: PoolParticipant) => new PoolParticipant(poolParticipant))));
  }

  public getNextPoolParticipantsToPick(quantity = ''): Observable<Map<number, string>> {
    return this.http.get<Map<number, string>>(this.getFullApiUrl(`pools/nextPoolParticipantsToPick/${quantity}`));
  }

  public addPoolParticipantToPool(poolerName: string): Observable<PoolParticipant> {
    return this.http
      .post<PoolParticipant>(this.getFullApiUrl('pools/poolParticipants'), { name: poolerName })
      .pipe(map((poolParticipant: PoolParticipant) => new PoolParticipant(poolParticipant)));
  }

  public renamePooler(pooler: Pooler, newPoolerName: string): Observable<Pooler> {
    const updatedPooler = new Pooler();
    updatedPooler.poolerGuid = pooler.poolerGuid;
    updatedPooler.name = newPoolerName;
    return this.http.put<Pooler>(this.getFullApiUrl('poolers'), updatedPooler);
  }

  public removePoolParticipantFromPool(poolerGuid: string): Observable<PoolParticipant[]> {
    return this.http
      .delete<PoolParticipant[]>(this.getFullApiUrl(`pools/poolParticipants/${poolerGuid}`))
      .pipe(map((poolParticipants: PoolParticipant[]) => poolParticipants.map((poolParticipant: PoolParticipant) => new PoolParticipant(poolParticipant))));
  }

  public changePoolParticipantPickPosition(poolerGuid: string, newPickPosition: number): Observable<PoolParticipant[]> {
    return this.http
      .put<PoolParticipant[]>(this.getFullApiUrl(`pools/poolParticipants/${poolerGuid}/pickOrder`), newPickPosition)
      .pipe(map((poolParticipants: PoolParticipant[]) => poolParticipants.map((poolParticipant: PoolParticipant) => new PoolParticipant(poolParticipant))));
  }

  public getAllPicksInPool(): Observable<{ [poolerGuid: string]: Player[] }> {
    return this.http.get<{ [poolerGuid: string]: Player[] }>(this.getFullApiUrl('pools/picks')).pipe(
      map((picks: { [poolerGuid: string]: Player[] }) => {
        Object.keys(picks).forEach((poolerGuid: string) => {
          picks[poolerGuid] = picks[poolerGuid].map((player: Player) => new Player(player));
        });
        return picks;
      })
    );
  }

  public pickPlayer(playerNhlId: string): Observable<KeyValuePair<string, Player[]>> {
    return this.http.post<KeyValuePair<string, Player[]>>(this.getFullApiUrl('pools/picks'), JSON.stringify(playerNhlId)).pipe(
      map((playerPicks: KeyValuePair<string, Player[]>) => {
        playerPicks.value = playerPicks.value.map((player: Player) => new Player(player));
        return playerPicks;
      })
    );
  }

  public undoLastPick(): Observable<KeyValuePair<string, Player[]>> {
    return this.http.delete<KeyValuePair<string, Player[]>>(this.getFullApiUrl('pools/picks')).pipe(
      map((playerPicks: KeyValuePair<string, Player[]>) => {
        playerPicks.value = playerPicks.value.map((player: Player) => new Player(player));
        return playerPicks;
      })
    );
  }

  public getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.getFullApiUrl('players')).pipe(map((players: Player[]) => players.map((player: Player) => new Player(player))));
  }

  public getPlayersPredictions(season: string): Observable<Player[]> {
    return this.http.get<Player[]>(this.getFullApiUrl(`players/predictions/${season}`)).pipe(map((players: Player[]) => players.map((player: Player) => new Player(player))));
  }

  public connectToSpectateHub(): void {
    if (!this.spectateHubConnection || this.spectateHubConnection.state === signalR.HubConnectionState.Disconnected) {
      this.spectateHubConnection = new signalR.HubConnectionBuilder()
        .withUrl(this.getFullWebsocketsUrl('spectate'), this.spectateHubConnectionOptions)
        .withAutomaticReconnect()
        .build();
      this.spectateHubConnection
        .start()
        .then(() => {
          // this.toastr.info('Connecté au hub des spectateurs');
          // console.log('Connecté au hub des spectateurs');
          this.initializeEvents();
          // this.spectateHubConnection.onclose((err) => console.log(`Déconnecté du hub des spectateurs : ${err}`));
        })
        .catch((err) => {
          this.toastr.error('Les spectateurs ne recevront pas les mises à jour.', 'Errur lors de la connexion au hub des spectateurs');
          console.log('Errur lors de la connexion au hub des spectateurs.Les spectateurs ne recevront pas les mises à jour. ' + err);
        });
    }
  }

  private initializeEvents() {
    this.spectateHubConnection.on('UpdatePoolParticipants', (poolParticipants: PoolParticipant[]) => {
      this.spectatePoolParticipants.next(poolParticipants);
    });
    this.spectateHubConnection.on('UpdatePoolParticipantPicks', (poolerGuid: string, poolerPicks: Player[], pickedPlayerId: string) => {
      const poolerPicksUpdate = new PoolerPicksUpdate();
      poolerPicksUpdate.poolerGuid = poolerGuid;
      poolerPicksUpdate.poolerPicks = poolerPicks.map((player: Player) => new Player(player));
      poolerPicksUpdate.pickedPlayerId = pickedPlayerId;
      this.spectatePicks.next(poolerPicksUpdate);
      this.spectatePicksCompleted.next(false);
    });
    this.spectateHubConnection.on('UpdatePickingPoolParticipant', (poolerGuid: string) => {
      this.spectatePickingPoolParticipant.next(poolerGuid);
    });
    this.spectateHubConnection.on('UpdateNextPickingPoolParticipant', (poolerGuid: string) => {
      this.spectateNextPickingPoolParticipant.next(poolerGuid);
    });

    this.spectateHubConnection.on('PicksCompleted', () => {
      this.spectatePicksCompleted.next(true);
    });
  }

  public updateSpectatorPoolParticipants(): void {
    if (!!this.spectateHubConnection && this.spectateHubConnection.state === signalR.HubConnectionState.Connected) {
      this.spectateHubConnection.send('UpdateSpectatorPoolParticipants');
    }
  }

  public updateSpectatorPicks(poolerGuid: string, pickedPlayerId: string): void {
    if (!!this.spectateHubConnection && this.spectateHubConnection.state === signalR.HubConnectionState.Connected) {
      this.spectateHubConnection.send('UpdateSpectatorPicks', poolerGuid, pickedPlayerId);
    }
  }

  public notifySpectatorsPicksCompleted(): void {
    if (!!this.spectateHubConnection && this.spectateHubConnection.state === signalR.HubConnectionState.Connected) {
      this.spectateHubConnection.send('NotifyPicksCompleted');
    }
  }

  public disconnectFromSpectateHub(): void {
    if (!!this.spectateHubConnection && this.spectateHubConnection.state === signalR.HubConnectionState.Connected) {
      this.spectateHubConnection.stop();
    }
  }

  private getFullWebsocketsUrl(resource: string): string {
    if (resource.indexOf('/') === 0) {
      resource = resource.substr(1);
    }
    return environment.websocketsBaseUrl + resource;
  }

  private getFullApiUrl(resource: string): string {
    if (resource.indexOf('/') === 0) {
      resource = resource.substr(1);
    }
    return environment.apiBaseUrl + resource;
  }
}
