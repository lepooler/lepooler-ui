import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ThemeService } from '../theme.service';

@Component({
    selector: 'app-theme-switcher',
    templateUrl: './theme-switcher.component.html',
})
export class ThemeSwitcherComponent implements OnDestroy {

  public isDarkTheme = false;

  private subscriptions: Subscription[] = [];

  constructor(public themeService: ThemeService) {
      this.subscriptions.push(this.themeService.themeChanged.subscribe(currentTheme => this.isDarkTheme = currentTheme.indexOf('dark') > -1));
  }

  ngOnDestroy(): void {
      this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }
}
