import { Component, Input, OnChanges } from '@angular/core';

import { Injury, Player } from '../../models/player';

@Component({
  selector: 'app-player-name',
  templateUrl: './player-name.component.html',
})
export class PlayerNameComponent implements OnChanges {
  @Input() public player: Player;

  public injury: Injury;

  public ngOnChanges(): void {
    if (this.player.activeInjuries?.length > 0) {
      this.injury = this.player.activeInjuries[0];
    }
  }
}
