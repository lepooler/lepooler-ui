import { environment } from '../../environments/environment.prod';

export class Utilities {
    public static getJwtToken(): string {
        let token = localStorage.getItem(environment.tokenStorageName);
        if (!token) {
            token = localStorage.getItem(environment.spectatingTokenStorageName);
        }
        return token;
    }
}
