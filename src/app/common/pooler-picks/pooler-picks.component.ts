import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import { Player } from '../../models/player';
import { PoolParticipant } from '../../models/pool-participant';

@Component({
    selector: 'app-pooler-picks',
    templateUrl: './pooler-picks.component.html'
})
export class PoolerPicksComponent {
  @Input() public isSpectating = false;
  @Input() public poolParticipant: PoolParticipant;
  @Input() public playerSelectionStarted = false;
  @Input() public isReport = false;
  @Input() public picking = false;
  @Input() public pickingNext = false;
  @Output() rename = new EventEmitter<string>();
  @Output() delete = new EventEmitter();
  public isCollapsed = true;
  public isRenaming = false;
  public newPoolerName: string;

  @ViewChild('renameInput') renameInput: ElementRef;

  public startRename(): void {
      this.isRenaming = true;
      setTimeout(() => {
          this.newPoolerName = this.poolParticipant.pooler.name;
          this.renameInput.nativeElement.select();
      }, 10);
  }

  public renameMe(): void {
      this.rename.emit(this.newPoolerName);
      this.isRenaming = false;
      this.newPoolerName = '';
  }

  public cancelRename(): void {
      this.isRenaming = false;
      this.newPoolerName = '';
  }

  public deleteMe(): void {
      if (confirm('Êtes-vous sûr de vouloir supprimer ' + this.poolParticipant.pooler.name + ' du pool?')) {
          this.delete.emit();
      }
  }

  public getPoolerPicksInRankOrder(poolParticipant: PoolParticipant) {
      if (poolParticipant.picks === undefined) {
          return []; // Picks not yet loaded. They should be loaded by the calling component.
      }
      return poolParticipant.picks.sort((a: Player, b: Player) => a.predictions[0].rank - b.predictions[0].rank);
  }
}
