import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html'
})
export class ModalComponent {
    @Input() public title: string;
    @Input() public modalClass = '';

    constructor(private modalService: NgbModal) { }

    public open(content) {
        this.modalService.open(content, { windowClass: this.modalClass});
    }
}
