import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { environment } from '../../environments/environment';
import { CurrentPoolDataService } from '../current-pool-data-service/current-pool-data.service';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html'
})
export class LandingComponent implements OnInit, OnDestroy {
  public action = 'create';
  public creatingOrAuthenticating = false;
  public isSpectating = false;

  private routeSnapshot: ActivatedRouteSnapshot;

  private subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private router: Router,
    public currentPoolDataService: CurrentPoolDataService) {
      this.routeSnapshot = this.route.snapshot;
  }

  ngOnInit() {
      this.clearExistingAuthentication();

      this.isSpectating = this.routeSnapshot.data.isSpectating;
      if (this.isSpectating) {
          this.action = 'spectate';
      }
  }

  private clearExistingAuthentication() {
      this.currentPoolDataService.setCurrentPool(undefined, false);
      localStorage.removeItem(environment.tokenStorageName);
      localStorage.removeItem(environment.spectatingTokenStorageName);
  }

  ngOnDestroy() {
      this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  public createdOrAuthenticated(): void {
      this.creatingOrAuthenticating = false;
      if (this.action === 'spectate') {
          this.router.navigate(['spectate']);
      } else {
          this.router.navigate(['playerSelection']);
      }
  }
}
