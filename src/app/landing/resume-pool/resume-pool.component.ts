import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';
import { PoolService } from '../../pool-service/pool.service';

@Component({
  selector: 'app-resume-pool',
  templateUrl: './resume-pool.component.html',
})
export class ResumePoolComponent implements OnInit, OnDestroy {
  @Output() authenticated = new EventEmitter();
  public resumePoolForm: FormGroup;
  public submitted = false;
  public availableSeasons: string[];

  @Output() private authenticating = new EventEmitter<boolean>();

  private subscriptions: Subscription[] = [];

  constructor(private poolService: PoolService) {}

  public ngOnInit(): void {
    this.subscriptions.push(
      this.poolService.getSeasons().subscribe((seasons: string[]) => {
        this.availableSeasons = seasons;
        this.resumePoolForm.controls['season'].setValue(seasons[0]);
      })
    );
    this.resumePoolForm = new FormGroup({
      season: new FormControl('', Validators.required),
      poolName: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required]),
    });
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  public get form(): { [key: string]: AbstractControl } {
    return this.resumePoolForm.controls;
  }

  public onSubmit(): void {
    this.submitted = true;

    if (this.resumePoolForm.invalid) {
      return;
    }

    this.authenticating.emit(true);
    this.subscriptions.push(
      this.poolService.authenticate(this.resumePoolForm.value).subscribe(
        (token: string) => {
          localStorage.setItem(environment.tokenStorageName, token);
          this.authenticated.emit();
        },
        () => this.authenticating.emit(false)
      )
    );
  }
}
