import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';
import { PoolService } from '../../pool-service/pool.service';

@Component({
  selector: 'app-create-pool',
  templateUrl: './create-pool.component.html',
})
export class CreatePoolComponent implements OnInit, OnDestroy {
  @Output() public created = new EventEmitter();
  public createPoolForm: FormGroup;
  public submitted = false;
  public availableSeasons: string[];

  @Output() private creating = new EventEmitter<boolean>();

  private subscriptions: Subscription[] = [];

  constructor(private poolService: PoolService) {}

  public ngOnInit(): void {
    this.subscriptions.push(
      this.poolService.getSeasons().subscribe((seasons: string[]) => {
        this.availableSeasons = seasons;
        this.createPoolForm.controls['season'].setValue(seasons[0]);
      })
    );
    this.createPoolForm = new FormGroup(
      {
        season: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required),
        nbGoalerPicks: new FormControl('', [Validators.required, Validators.min(0), Validators.max(20)]),
        nbOffenseDefensePicks: new FormControl('', [Validators.required, Validators.min(0), Validators.max(99)]),
        pickTimeLimitInSeconds: new FormControl('', [Validators.required, Validators.min(30), Validators.max(999)]),
        password: new FormControl('', [Validators.required, Validators.minLength(6)]),
        passwordConfirmation: new FormControl('', [Validators.required, Validators.minLength(6)]),
      },
      this.validatePasswordConfirmation
    );
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  public get form() {
    return <any>this.createPoolForm.controls;
  }
  public get formErrors() {
    return <any>this.createPoolForm.errors;
  }

  public onSubmit(): void {
    this.submitted = true;

    if (this.createPoolForm.invalid) {
      return;
    }

    this.creating.emit(true);
    this.subscriptions.push(
      this.poolService.createPool(this.createPoolForm.value).subscribe(
        (token: string) => {
          localStorage.setItem(environment.tokenStorageName, token);
          this.created.emit();
        },
        () => this.creating.emit(false)
      )
    );
  }

  private validatePasswordConfirmation(createPoolForm: FormGroup) {
    const password = createPoolForm.controls.password.value;
    const passwordConfirmation = createPoolForm.controls.passwordConfirmation.value;
  
    if (passwordConfirmation.length <= 0) {
      return null;
    }
  
    if (passwordConfirmation !== password) {
      return {
        doesntMatchPassword: true,
      };
    }
    return null;
  }
}
