import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';
import { PoolService } from '../../pool-service/pool.service';

@Component({
    selector: 'app-spectate-pool',
    templateUrl: './spectate-pool.component.html'
})
export class SpectatePoolComponent implements OnInit, OnDestroy {
  @Output() authenticated = new EventEmitter();
  @Output() private authenticating = new EventEmitter<boolean>();

  public invalidLink = false;

  private routeSnapshot: ActivatedRouteSnapshot;
  private subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private poolService: PoolService) {
      this.routeSnapshot = this.route.snapshot;
  }

  ngOnInit() {
      this.authenticating.emit(true);
      this.subscriptions.push(this.poolService.authenticateSpectating(this.routeSnapshot.paramMap.get('poolGuid')).subscribe((token: string) => {
          localStorage.setItem(environment.spectatingTokenStorageName, token);
          this.authenticated.emit();
      }, () => {
          this.invalidLink = true;
          this.authenticating.emit(false);
      }));
  }

  ngOnDestroy() {
      this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }
}
