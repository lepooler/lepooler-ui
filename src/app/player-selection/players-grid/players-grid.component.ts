import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

import { Constants } from '../../common/constants';
import { Player } from '../../models/player';
import { ThemeService } from '../../theme/theme.service';
import { PlayerSelectionDataService } from '../player-selection-data-service/player-selection-data.service';

/* eslint-disable @typescript-eslint/no-unused-vars */
@Component({
  selector: 'app-players-grid',
  templateUrl: './players-grid.component.html',
})
export class PlayersGridComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() public isSpectating = false;
  public players: Player[] = [];
  public gridLoading = true;

  @Output() public playerPicked = new EventEmitter<Player>();

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;

  private showGoalers = true;
  private showOffense = true;
  private showDefense = true;
  private showPickedPlayers = false;

  private dtLanguageSettings = {
    processing: 'Traitement en cours...',
    search: 'Rechercher :',
    lengthMenu: 'Afficher _MENU_ joueurs',
    info: 'Affichage des joueurs _START_ &agrave; _END_ sur _TOTAL_ joueurs',
    infoEmpty: 'Aucun joueur affiché',
    infoFiltered: '(filtr&eacute; de _MAX_ joueurs au total)',
    infoPostFix: '',
    loadingRecords: 'Chargement en cours...',
    zeroRecords: 'Aucun joueur &agrave; afficher',
    emptyTable: 'Aucune donn&eacute;e disponible dans le tableau',
    paginate: {
      first: '<<',
      previous: '<',
      next: '>',
      last: '>>',
    },
    aria: {
      sortAscending: ': activer pour trier la colonne par ordre croissant',
      sortDescending: ': activer pour trier la colonne par ordre d&eacute;croissant',
    },
    buttons: {
      pageLength: {
        _: 'Afficher...',
        '-1': 'Tous affichés',
      },
    },
  };
  public dtOptions = {
    deferRender: true,
    pageLength: 50,
    lengthMenu: [
      [25, 50, 100, -1],
      ['25 joueurs', '50 joueurs', '100 joueurs', 'Tous les joueurs'],
    ],
    language: this.dtLanguageSettings,
    order: [[3, 'asc']],
    columnDefs: [
      {
        orderable: false,
        targets: [0, 2],
      },
      {
        searchable: false,
        targets: [3, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
      },
      {
        visible: false,
        targets: [1, 16],
      },
    ],
    // Declare the use of the 'buttons' extension (Replace 'l'/length by 'B'/Button ); see styling section at https://datatables.net/reference/option/dom
    dom: '<"row ml-1"<"col-12 col-lg-3"f><"col-12 col-lg-9"B>><"row"<"col-12"tr>><"row ml-1"<"col-12"i><"col-12"p>>',
    buttons: [
      function (dataTable: DataTables.DataTables) {
        const lengthMenu = dataTable.settings()[0].aLengthMenu;
        const vals = Array.isArray(lengthMenu[0]) ? lengthMenu[0] : lengthMenu;
        const lang = Array.isArray(lengthMenu[0]) ? lengthMenu[1] : lengthMenu;
        const text = function (dt: DataTables.DataTables) {
          return dt.i18n(
            'buttons.pageLength',
            {
              '-1': 'Show all rows',
              _: 'Show %d rows',
            },
            dt.page.len()
          );
        };

        return {
          extend: 'collection',
          text: text,
          className: 'buttons-page-length btn btn-primary',
          autoClose: true,
          buttons: $.map(vals, function (val, i) {
            return {
              text: lang[i],
              className: 'button-page-length btn btn-outline-dark',
              action: function (e, dt: DataTables.DataTables) {
                dt.page.len(val).draw();
              },
              init: function (dt: DataTables.DataTables, node, conf) {
                const fn = function () {
                  this.active(dt.page.len() === val);
                }.bind(this);

                dt.on('length.dt' + conf.namespace, fn);
                fn();
                $(node).removeClass('dt-button');
              },
              destroy: function (dt: DataTables.DataTables, node, conf) {
                dt.off('length.dt' + conf.namespace);
              },
            };
          }),
          init: function (dt: DataTables.DataTables, node, conf) {
            dt.on(
              'length.dt' + conf.namespace,
              function () {
                this.text(text(dt));
              }.bind(this)
            );
            $(node).removeClass('dt-button');
          },
          destroy: function (dt: DataTables.DataTables, node, conf) {
            dt.off('length.dt' + conf.namespace);
          },
        };
      },
      {
        text: 'Gardiens',
        className: 'btn btn-outline-dark active',
        action: this.toggleShowGoalers.bind(this),
        init: function (api, node, config) {
          $(node).removeClass('dt-button');
        },
      },
      {
        text: 'Attaquants',
        className: 'btn btn-outline-dark active',
        action: this.toggleShowOffense.bind(this),
        init: function (api, node, config) {
          $(node).removeClass('dt-button');
        },
      },
      {
        text: 'Défenseurs',
        className: 'btn btn-outline-dark active',
        action: this.toggleShowDefense.bind(this),
        init: function (api, node, config) {
          $(node).removeClass('dt-button');
        },
      },
      {
        text: 'Joueurs déjà choisis',
        className: 'btn btn-outline-dark',
        action: this.toggleShowPickedPlayers.bind(this),
        init: function (api, node, config) {
          $(node).removeClass('dt-button');
        },
      },
    ],
    preDrawCallback: this.gridIsLoading.bind(this),
    drawCallback: this.gridDoneLoading.bind(this),
    initComplete: this.tableInitComplete.bind(this),
  };
  public dtTrigger = new Subject();

  public constructor(private playerSelectionDataService: PlayerSelectionDataService, public themeService: ThemeService) {}

  private filtersFunction: (settings, data, dataIndex, rowData, counter) => boolean;

  public ngOnInit(): void {
    this.filtersFunction = this.applyFilters.bind(this);
    $.fn['dataTable'].ext.search.push(this.filtersFunction);
  }

  public ngAfterViewInit(): void {
    this.playerSelectionDataService.players$.subscribe((players: Player[]) => {
      this.players = players;
      this.refresh();
    });
  }

  private applyFilters(settings, data, dataIndex, rowData, counter): boolean {
    const pickedPlayerFlag = data[1];
    const playerPosition = data[4];
    return this.isPositionShown(playerPosition) && this.isPlayerNotPickedOrPickedPlayersShown(pickedPlayerFlag);
  }

  public ngOnDestroy(): void {
    $.fn['dataTable'].ext.search = $.fn['dataTable'].ext.search.filter((p) => p !== this.filtersFunction);
    this.dtTrigger.unsubscribe();
  }

  public isPositionShown(playerPosition: string): boolean {
    return (playerPosition === 'Gardien' && this.showGoalers) || (playerPosition === 'Attaquant' && this.showOffense) || (playerPosition === 'Défenseur' && this.showDefense);
  }

  public isPlayerNotPickedOrPickedPlayersShown(columnData: string): boolean {
    return columnData.indexOf('true') === -1 || this.showPickedPlayers;
  }

  public getNhlTeamId(teamAbbreviation: string): string {
    if (!!teamAbbreviation && Constants.teamAbbrNhlIdMapping.has(teamAbbreviation.toUpperCase())) {
      return Constants.teamAbbrNhlIdMapping.get(teamAbbreviation.toUpperCase()).toString();
    }

    return 'unknown';
  }

  public tableInitComplete() {}

  public refresh() {
    if (this.dtElement.dtInstance) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        if (dtInstance.rows().length === 0) {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        } else {
          dtInstance.rows().invalidate();
          dtInstance.draw();
        }
      });
    } else {
      this.dtTrigger.next();
    }
  }

  private toggleShowGoalers(e, dt, node, config): void {
    this.showGoalers = !this.showGoalers;
    if (this.showGoalers) {
      $(node).addClass('active');
    } else {
      $(node).removeClass('active');
    }
    this.refresh();
  }

  private toggleShowOffense(e, dt, node, config): void {
    this.showOffense = !this.showOffense;
    if (this.showOffense) {
      $(node).addClass('active');
    } else {
      $(node).removeClass('active');
    }
    this.refresh();
  }

  private toggleShowDefense(e, dt, node, config): void {
    this.showDefense = !this.showDefense;
    if (this.showDefense) {
      $(node).addClass('active');
    } else {
      $(node).removeClass('active');
    }
    this.refresh();
  }

  private toggleShowPickedPlayers(e, dt, node, config): void {
    this.showPickedPlayers = !this.showPickedPlayers;
    if (this.showPickedPlayers) {
      $(node).addClass('active');
    } else {
      $(node).removeClass('active');
    }
    this.refresh();
  }

  public gridIsLoading(): void {
    this.setGridLoading(true);
  }

  public gridDoneLoading(): void {
    this.setGridLoading(false);
  }

  public setGridLoading(gridLoading: boolean): void {
    this.gridLoading = gridLoading;
  }
}
