import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

import { KeyValuePair } from '../../common/key-value-pair';
import { Player } from '../../models/player';
import { PoolParticipant } from '../../models/pool-participant';
import { Pooler } from '../../models/pooler';

@Injectable({
  providedIn: 'root',
})
export class PlayerSelectionDataService {
  public readonly poolParticipants = new Map<string, PoolParticipant>();
  private poolParticipantsInPickOrder: PoolParticipant[];
  public readonly players = new Map<string, Player>();

  private poolParticipantsSubject = new ReplaySubject<PoolParticipant[]>();
  public poolParticipants$ = this.poolParticipantsSubject.asObservable();

  private playersSubject = new ReplaySubject<Player[]>();
  public players$ = this.playersSubject.asObservable();

  public updateData(players: Player[], poolParticipants: PoolParticipant[], picks: { [poolerGuid: string]: Player[] }): void {
    if (players) {
      players.forEach((player: Player) => {
        this.players.set(player.playerNhlId, player);
      });
      this.playersSubject.next(Array.from(this.players.values()));
    }

    if (poolParticipants) {
      this.updatePoolersMap(poolParticipants);
    }

    if (this.players && this.poolParticipants && picks) {
      Object.keys(picks).forEach((poolerGuid: string) => {
        this.poolParticipants.get(poolerGuid).picks = picks[poolerGuid];
        picks[poolerGuid].forEach((player: Player) => {
          this.players.get(player.playerNhlId).picked = true;
          this.players.get(player.playerNhlId).pickedBy = this.poolParticipants.get(poolerGuid).pooler.name;
        });
      });
      this.playersSubject.next(Array.from(this.players.values()));
    }
  }

  public updatePoolersMap(updatedPoolParticipants: PoolParticipant[]): void {
    this.poolParticipants.clear();
    this.poolParticipantsInPickOrder = null;
    updatedPoolParticipants.forEach((poolParticipant) => {
      this.poolParticipants.set(poolParticipant.pooler.poolerGuid, poolParticipant);
    });
    this.updatePickOrder();
  }

  private updatePickOrder(): void {
    this.poolParticipantsInPickOrder = Array.from(this.poolParticipants.values()).sort((a: PoolParticipant, b: PoolParticipant) => a.pickOrderPosition - b.pickOrderPosition);
    this.poolParticipantsSubject.next(this.poolParticipantsInPickOrder);
  }

  public addPooler(newPoolParticipant: PoolParticipant): void {
    this.updatePoolersMap(this.poolParticipantsInPickOrder.concat([newPoolParticipant]));
  }

  public renamePooler(updatedPooler: Pooler): void {
    this.poolParticipants.get(updatedPooler.poolerGuid).pooler.name = updatedPooler.name;
    this.updatePoolersMap(this.poolParticipantsInPickOrder);
  }

  public updatePoolerPicks(poolerPicks: KeyValuePair<string, Player[]>, pickedPlayerId: string): void {
    this.poolParticipants.get(poolerPicks.key).picks = poolerPicks.value;
    this.players.get(pickedPlayerId).picked = !this.players.get(pickedPlayerId).picked;
    this.players.get(pickedPlayerId).pickedBy = this.poolParticipants.get(poolerPicks.key).pooler.name;
  }
}
