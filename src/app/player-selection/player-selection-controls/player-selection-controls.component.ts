import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Howl } from 'howler';
import { CountdownComponent } from 'ngx-countdown';

@Component({
    selector: 'app-player-selection-controls',
    templateUrl: './player-selection-controls.component.html'
})
export class PlayerSelectionControlsComponent {
  @Input() public countdownTimeInSeconds: number;
  @Input() public playerSelectionCompleted: boolean;
  @Input() public isSpectating = false;
  @Output() public pickTimeElapsed = new EventEmitter();
  @Output() public undoLastPick = new EventEmitter();
  public countdownAlmostFinished = false;
  public paused = false;

  @ViewChild(CountdownComponent) countdown: CountdownComponent;

  constructor(private router: Router) { }

  public restart(): void {
      this.countdownAlmostFinished = false;
      if (this.countdown !== undefined) {
          if (!this.playerSelectionCompleted) {
              this.countdown.restart();
              this.paused = false;
          } else {
              this.countdown.stop();
              this.paused = true;
          }
      }
  }

  public resumeCountdown() {
      if (this.countdown !== undefined) {
          this.countdown.resume();
      }
      this.paused = false;
  }

  public stopCountdown() {
      if (this.countdown !== undefined) {
          this.countdown.pause();
      }
      this.paused = true;
  }

  public countdownFinished() {
      new Howl({ src: ['assets/hockey_buzzer.mp3'], volume: 0.25  }).play();
      this.pickTimeElapsed.emit();
  }

  public navigateToReport(): void {
      if (this.isSpectating) {
          this.router.navigate(['/spectateReport']);
      } else {
          this.router.navigate(['/report']);
      }
  }
}
