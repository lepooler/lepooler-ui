import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddNewPoolerComponent } from './add-new-pooler.component';

describe('AddNewPoolerComponent', () => {
    let component: AddNewPoolerComponent;
    let fixture: ComponentFixture<AddNewPoolerComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ AddNewPoolerComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddNewPoolerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
