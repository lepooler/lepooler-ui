import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { SortablejsOptions } from 'ngx-sortablejs';
import { Subscription } from 'rxjs';

import { PoolParticipant } from '../../models/pool-participant';
import { Pooler } from '../../models/pooler';
import { PoolService } from '../../pool-service/pool.service';
import { PlayerSelectionDataService } from '../player-selection-data-service/player-selection-data.service';

@Component({
  selector: 'app-poolers-list',
  templateUrl: './poolers-list.component.html',
})
export class PoolersListComponent implements OnInit, OnDestroy {
  @Input() public playerSelectionStarted = false;
  @Input() public isSpectating = false;
  //@Output() updatePoolers = new EventEmitter<PoolParticipant[]>();
  @Output() picksCompleted = new EventEmitter();
  public poolParticipantsInPickOrder: PoolParticipant[];
  public pickingPoolerGuid: string;
  public nextPickingPoolerGuid: string;

  public sortableJsOptions: SortablejsOptions = {
    dataIdAttr: 'data-id',
    animation: 150,
    onUpdate: (event: any) => {
      this.changePoolerPosition(event);
    },
  };

  private subscriptions: Subscription[] = [];

  constructor(private poolService: PoolService, private playerSelectionDataService: PlayerSelectionDataService) {}

  public ngOnInit(): void {
    this.subscriptions.push(
      this.playerSelectionDataService.poolParticipants$.subscribe((poolParticipants: PoolParticipant[]) => {
        this.poolParticipantsInPickOrder = poolParticipants;
        if (!this.isSpectating) {
          this.poolService.updateSpectatorPoolParticipants();
        }
        this.updatePickingPooler();
      })
    );

    if (this.isSpectating) {
      this.subscriptions.push(
        this.poolService.spectatePickingPoolParticipant$.subscribe((poolerGuid: string) => {
          this.pickingPoolerGuid = poolerGuid;
        })
      );
      this.subscriptions.push(
        this.poolService.spectateNextPickingPoolParticipant$.subscribe((poolerGuid: string) => {
          this.nextPickingPoolerGuid = poolerGuid;
        })
      );
    }
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  public updatePickingPooler(): void {
    if (!this.isSpectating || !this.pickingPoolerGuid) {
      this.subscriptions.push(
        this.poolService.getNextPoolParticipantsToPick('2').subscribe((nextPoolersToPick: Map<number, string>) => {
          // if none, emit picks done so we stop the timer and replace it with a button to go to the report
          if (nextPoolersToPick === undefined || nextPoolersToPick[1] === undefined) {
            this.picksCompleted.emit();
            this.resetPickingPooler();
          } else {
            this.pickingPoolerGuid = nextPoolersToPick[1];
            this.nextPickingPoolerGuid = nextPoolersToPick[2];
          }
        })
      );
    }
  }

  private resetPickingPooler(): void {
    this.pickingPoolerGuid = undefined;
    this.nextPickingPoolerGuid = undefined;
  }

  public renamePooler(poolParticipant: PoolParticipant, newPoolerName: string): void {
    this.subscriptions.push(
      this.poolService.renamePooler(poolParticipant.pooler, newPoolerName).subscribe((updatedPooler: Pooler) => {
        this.playerSelectionDataService.renamePooler(updatedPooler);
      })
    );
  }

  public removePooler(poolParticipant: PoolParticipant): void {
    this.subscriptions.push(
      this.poolService.removePoolParticipantFromPool(poolParticipant.pooler.poolerGuid).subscribe((poolersWithNewPickOrder: PoolParticipant[]) => {
        this.playerSelectionDataService.updatePoolersMap(poolersWithNewPickOrder);
      })
    );
  }

  public changePoolerPosition(event: any): void {
    const movedPoolerGuid = event.item.getAttribute(this.sortableJsOptions.dataIdAttr);
    this.subscriptions.push(
      this.poolService.changePoolParticipantPickPosition(movedPoolerGuid, event.newIndex + 1).subscribe((poolersWithNewPickOrder: PoolParticipant[]) => {
        this.playerSelectionDataService.updatePoolersMap(poolersWithNewPickOrder);
      })
    );
  }
}
