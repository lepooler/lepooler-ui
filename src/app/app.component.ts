import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { CurrentPoolDataService } from './current-pool-data-service/current-pool-data.service';
import { Pool } from './models/pool';
import { PoolService } from './pool-service/pool.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnDestroy {
  public title = 'LePooler';
  public currentPool: Pool;

  private subscriptions: Subscription[] = [];

  constructor(private currentPoolDataService: CurrentPoolDataService, private poolService: PoolService) {
      this.subscriptions.push(this.currentPoolDataService.currentPool$.subscribe((currentPool: Pool) => this.currentPool = currentPool));
  }

  ngOnDestroy() {
      if (this.currentPoolDataService.isSpectating) {
          this.poolService.disconnectFromSpectateHub();
      }
      this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  onRouteChange() {
      Array.from(document.getElementsByTagName('tooltip')).forEach(item => item.parentNode.removeChild(item));
  }
}
