#!/bin/bash


if [ -n "$JWT_WHITELISTED_DOMAIN" ]; then
    sed -i "s#REPLACED_BY_ENV_JWT_WHITELISTED_DOMAIN#$JWT_WHITELISTED_DOMAIN#g" /usr/share/nginx/html/main*.js
fi
if [ -n "$API_BASE_URL" ]; then
    sed -i "s#REPLACED_BY_ENV_API_BASE_URL#$API_BASE_URL#g" /usr/share/nginx/html/main*.js
fi
echo "Starting nginx..."
nginx -g 'daemon off;'